import 'package:flutter/material.dart';

import 'manage_client.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Gabber Admin Panel',
      theme: ThemeData(
        primarySwatch: Colors.green,
        //scaffoldBackgroundColor: Colors.white38,
      ),
      home: const MyHomePage(title: 'Gabber Admin Panel'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;

  const MyHomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title), actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.web_sharp),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ManageClient()),
            );
          },
        ),
        PopupMenuButton(itemBuilder: (BuildContext context) {
          return [
            const PopupMenuItem(child: Text("Иди нахуй пупсик")),
          ];
        }),
      ]),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Список подключеннх"),
            //ListView.builder(
            //    itemCount: 4,
            //    padding: const EdgeInsets.all(16.0),
            //    itemBuilder: (BuildContext context, int i) {
            //      return ListTile(
            //        leading: CircleAvatar(
            //          child: Text('-> '),
            //        ),
            //        title: Text(
            //          'Item ',
            //          style: TextStyle(fontSize: 18.0),
            //        ),
            //        trailing: const Icon(Icons.dashboard),
            //      );
            //    })
          ],
        ),
      ),
    );
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }
}
