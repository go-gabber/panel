import 'package:flutter/material.dart';

class ManageClient extends StatelessWidget {
  String _number = "", _text = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Въебать конкретного лолха: \$LOH_DIR"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _numberField(),
        _textField(),
        Divider(),
            ElevatedButton(
              child: Text("Отправить !"), onPressed: (){

                (_number == "") | (_text == "") ? 
                    print("API ERROR : Данные поправь сучина") :
                    print("POST GABBER://$_number@$_text");
              },
                ),
          ],
        ),
      ),
    );
  }

  Widget _numberField() {
    return TextFormField(
      textCapitalization: TextCapitalization.words,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        filled: true,
        icon: Icon(Icons.phone),
        hintText: "Введите номер",
        labelText: "Номер *",
      ),
      onChanged: (value) {
         _number = value.toString();
      },
    );
  }

  Widget _textField() {
    return TextFormField(
      textCapitalization: TextCapitalization.words,
      decoration: const InputDecoration(
        border: UnderlineInputBorder(),
        filled: true,
        icon: Icon(Icons.mail),
        hintText: "Введите сообщение",
        labelText: "Текст *",
      ),
      onChanged: (value) {
         _text = value.toString();
      },
    );
  }
}
